<?php
/**
 * @file
 * Template for random recepies links.
 */
?>
<div id="at-wt-calculator-recepies-random-links-wrapper">
  <?php if (!empty($random_ids)) : ?>
    <div class="list-group">
      <?php foreach ($random_ids as $id) : ?>
        <?php
          $text = t('Recepie') . ' #' . $id;
          $url  = drupal_get_path_alias('node/' . $id);
          $params = array(
            'attributes' => array(
              'class' => array(
                'list-group-item',
              ),
            ),
          );
          print l($text, $url, $params); ?>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
