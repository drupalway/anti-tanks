<?php
/**
 * @file
 * Template for Calculator Share page.
 */
?>
<div class="calculator-share-page">
  <?php if (!empty($wasted_time)) : ?>
    <div class="wasted-time">
      <h2 class="wasted-time-header"><?php print t('Wasted time:'); ?></h2>
      <div class="wasted-time-value">
        <?php print $wasted_time . (($wasted_time > 1) ? ' days' : ' day'); ?>
      </div>
    </div>
  <?php endif; ?>
  <?php if (!empty($lost_money)) : ?>
    <div class="lost-money"><?php print $lost_money; ?></div>
  <?php endif; ?>
  <?php if (!empty($conclusion)) : ?>
    <div class="conclusion"><?php print $conclusion; ?></div>
  <?php endif; ?>
  <?php if (!empty($recepies)) : ?>
    <div class="recepies"><?php print $recepies; ?></div>
  <?php endif; ?>
  <?php if (!empty($social)) : ?>
    <div class="social-buttons">
      <?php print $social; ?>
    </div>
  <?php endif; ?>
</div>
