(function($) {
  /**
   * Plus/minus buttons handler.
   */
  function get_crementor(input, crementors) {
    if (input.attr('name') == 'count_money_input') {
      return crementors[$('select[name="count_money_currency"]').val()];
    }
    else {
      return crementors[input.attr('name')];
    }
  }

  Drupal.behaviors.upDown = {
    attach: function (context, settings) {
      settings.at_calculator_crementors = settings.at_calculator_crementors || {};
      $('#at-wt-calculator-form-left-side-wrapper #up', context).each(function() {
        var input = $(this).prev().find('input.form-text');
        if (input.length == 0) {
          input = $('#edit-count-money-input');
        }
        $(this).off('click').on('click', function() {
          input.val(parseInt(input.val()) + get_crementor(input, settings.at_calculator_crementors));
          return false;
        });
      });
      $('#at-wt-calculator-form-left-side-wrapper #down', context).each(function() {
        var input = $(this).next().find('input');
        $(this).off('click').on('click', function() {
          if (parseInt(input.val()) - get_crementor(input, settings.at_calculator_crementors) >= 0) {
            input.val(parseInt(input.val()) - get_crementor(input, settings.at_calculator_crementors));
          }
          else {
            input.val(0);
          }
          return false;
        });
      });
    }
  };

  /**
   * Show/Hide Money form elements handler.
   *
   * @type {{attach: Function}}
   */
  Drupal.behaviors.showHideMoneyCounter = {
    attach: function (context) {
      var $left_wrapper = $('#at-wt-calculator-form-left-side-wrapper', context);
      $left_wrapper.find('input[name="count_lost_money"]').on('change', function() {
        if (this.checked) {
          $left_wrapper.find('.count-money-input').show();
        }
        else {
          $left_wrapper.find('.count-money-input').hide();
        }
      });
      $(document).ajaxSuccess(function() {
        $left_wrapper.find('input[name="count_lost_money"]').trigger('change');
      });
    }
  }
})(jQuery);
