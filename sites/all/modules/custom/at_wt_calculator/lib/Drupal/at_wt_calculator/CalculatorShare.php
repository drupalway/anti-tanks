<?php
/**
 * @file
 * CalculatorShare class file definition.
 */

namespace Drupal\at_wt_calculator;
/**
 * Class CalculatorShare.
 *
 * Create a custom database table and save into it a calculator results.
 *
 * @package Drupal\at_wt_calculator
 */
class CalculatorShare {
  /**
   * Static variable with a database table name.
   *
   * @var string
   */
  static protected $tableName = 'at_wt_calculator';
  /**
   * Array with a data to share.
   *
   * @var array
   */
  private $data = array();
  /**
   * Hash id.
   *
   * Length 12 symbols.
   *
   * @var string
   */
  private $hashId = '';
  /**
   * Data array form DB to share.
   *
   * @var array
   */
  private $pageData = array();
  /**
   * Hash ID to share.
   *
   * @var string
   */
  private $pageHash = '';

  /**
   * Create hash ID recursively.
   */
  private function createHashId() {
    $this->hashId = self::hashGenerator();
    if (self::checkUniqueHash($this->hashId)) {
      $this->createHashId();
    }
    $this->pageHash = $this->hashId;
  }

  /**
   * Generate random hash string.
   *
   * @return string
   *   Generated hash string.
   */
  protected static function hashGenerator() {
    return substr(md5(rand()), 0, 12);
  }

  /**
   * Check is generated Hash ID is unique.
   *
   * @param string $hash
   *   Hash to check for uniqueness.
   *
   * @return bool
   *   TRUE/FALSE.
   */
  private static function checkUniqueHash($hash) {
    $select = db_select(static::$tableName, 'c')
      ->fields('c', array('calc_id'))
      ->condition('calc_id', $hash);
    return $select->execute()->rowCount() > 0;
  }

  /**
   * Getting hash ID.
   *
   * @return string
   *   Hash ID.
   */
  private function getHashId() {
    return $this->hashId;
  }

  /**
   * Base construct method.
   *
   * @param string $action
   *   Action type, can be "read" or "write".
   * @param string $id
   *   Hash ID. Optional parameter for the "read" action.
   * @param array $conf
   *   Data array. Optional parameter for the "write" action.
   */
  public function __construct($action, $id = NULL, $conf = array()) {
    if ($action == 'read' && !empty($id)) {
      $this->calculatorRead($id);
    }
    elseif ($action == 'write' && !empty($conf)) {
      $this->calculatorWrite($conf);
    }
  }

  /**
   * Get the data array from the database.
   *
   * @param string $hash
   *   Hash ID.
   */
  private function getData($hash) {
    if (is_string($hash)) {
      $select = db_select(static::$tableName, 'c')
        ->fields('c', array('calc_data'))
        ->condition('calc_id', $hash);
      $this->data = $select->execute()->fetchField();
    }
  }

  /**
   * Filter input hash id to improve security protection.
   *
   * @param string $hash
   *   Input hash ID.
   *
   * @return mixed|string
   *   Filtered Hash ID string.
   */
  private static function filterInputHash($hash) {
    return filter_xss(check_plain($hash));
  }

  /**
   * Store data array to the database.
   */
  private function setData() {
    $hash = $this->getHashId();
    if (is_string($hash)) {
      db_insert(static::$tableName)
        ->fields(array('calc_id' => $hash, 'calc_data' => $this->data))
        ->execute();
    }
  }

  /**
   * Set serialised data array to the $this->data param.
   *
   * @param array $conf
   *   Data array.
   */
  private function setConfig(array $conf) {
    $this->data = self::configSerialise($conf);
  }

  /**
   * Get un-serialised data array from the $this->data param.
   */
  private function getConfig() {
    $this->pageData = self::configUnSerialise($this->data);
  }

  /**
   * Serialise data array.
   *
   * @param array $conf
   *   Un-serialised data array.
   *
   * @return string
   *   Serialised data array.
   */
  private static function configSerialise(array $conf) {
    return serialize($conf);
  }

  /**
   * Un-serialise data array.
   *
   * @param string $value
   *   Serialised data array.
   *
   * @return mixed
   *   Un-serialised data array.
   */
  private static function configUnSerialise($value) {
    return is_string($value) ? unserialize($value) : $value;
  }

  /**
   * Calculator write handler.
   *
   * @param array $conf
   *   Data array.
   */
  private function calculatorWrite(array $conf) {
    $this->createHashId();
    $this->setConfig($conf);
    $this->setData();
  }

  /**
   * Calculator read handler.
   *
   * @param string $id
   *   Hash ID.
   */
  private function calculatorRead($id) {
    $this->getData(self::filterInputHash($id));
    $this->getConfig();
  }

  /**
   * Get Page Hash.
   */
  public function getPageHash() {
    return $this->pageHash;
  }

  /**
   * Get Page Data.
   */
  public function getPageData() {
    return $this->pageData;
  }

}
