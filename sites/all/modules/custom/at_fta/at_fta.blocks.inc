<?php
/**
 * @file
 * FTA blocks.
 */

// Include the at_fta.module file.
module_load_include('module', 'at_fta', 'at_fta');

/**
 * Implements hook_block_info().
 */
function at_fta_block_info() {
  $block = array();
  $block['fta'] = array(
    'info' => t('FTA'),
  );
  return $block;
}

/**
 * Implements hook_block_view().
 */
function at_fta_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'fta':
      $block['subject'] = t('AntiTanks Advices');
      $block['content'] = at_fta_block_content($delta);
      return $block;

    default:
      return $block;
  }
}

/**
 * Display content in the block.
 *
 * @param string $delta
 *   Block's delta.
 *
 * @return array|null
 *   Render-able block array.
 */
function at_fta_block_content($delta) {
  switch ($delta) {
    case 'fta':
      $node = node_load(_at_fta_get_random_advice(0, 0, TRUE));
      $node->body['und'][0]['safe_value'] .= l(t('Show more'), '/advice');
      return node_view($node);

    default:
      return NULL;
  }
}
