<?php
/**
 * @file
 * AntiTanks FTA share form.
 */

/**
 * FTA custom node add form.
 */
function at_fta_form($form, &$form_state) {
  // Define the basic variables.
  global $user; static $type = 'fta';
  // Include the main Node's module files/handlers.
  module_load_include('inc', 'node', 'node.pages');
  form_load_include($form_state, 'inc', 'node', 'node.pages');
  // Load node if node ID exist, build the object otherwise.
  if (!empty($form_state['object']->fta_nid)) {
    $node = node_load($form_state['object']->fta_nid);
  }
  else {
    $node           = new stdClass();
    $node->uid      = $user->uid;
    $node->name     = isset($user->name) ? $user->name : '';
    $node->type     = $type;
    $node->language = LANGUAGE_NONE;
  }
  // Retrieve the node form.
  $form_state['build_info']['args'] = array($node);
  $form = drupal_retrieve_form($type . '_node_form', $form_state);
  // Disable access for few fields.
  $form['field_audio_track']['#access']   = 0;
  $form['field_fta_status']['#access']    = 0;
  $form['field_battle_range']['#access']  = 0;
  return $form;
}

/**
 * Implements hook_node_insert().
 */
function at_fta_node_insert($node) {
  if (isset($node->type) && $node->type == 'fta') {
    drupal_set_message('Thanks for your advice!');
    drupal_goto('advice/share');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function at_fta_form_at_fta_form_alter(&$form, &$form_state, $form_id) {
  $form['#after_build'][] = 'at_fta_node_form_after_build';
}

/**
 * FTA node form after build callback.
 */
function at_fta_node_form_after_build($form) {
  $form['body']['und']['0']['format']['#access'] = 0;
  return $form;
}
