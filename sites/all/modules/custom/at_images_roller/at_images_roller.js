(function($) {
  /**
   * Set an active class to the current image in roll.
   *
   * @type {{attach: Function}}
   */
  Drupal.behaviors.setActiveImageInRoller = {
    attach: function(context) {
      $(window).load(function() {
        var $roller = $('div.rollers-wrapper', context);
        if ($roller.length) {
          var path = this.location.pathname;
          $roller.find('div.ds-1col.node-demotivators.view-mode-at_dem').each(function() {
            var about_data = $(this).attr('about');
            if (path == about_data) {
              $(this).addClass('active-image-roll');
            }
          });
        }
      });
    }
  };

  Drupal.behaviors.imageRollerPrevNext = {
    attach: function(context, settings) {
      $(window).load(function() {
        var $selector = $('.ds-1col.node.node-demotivators.view-mode-full', context);
        if ($selector.length) {
          var links_html = '<div class="prev_next">' + settings.at_images_prev_next.prev
            + settings.at_images_prev_next.next + '</div>';
          $selector.find('> .field-name-field-image').prepend(links_html);
        }
      });
    }
  };
})(jQuery);
