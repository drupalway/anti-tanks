<?php
/**
 * @file
 * AntiTanks Social Share buttons template.
 */
?>
<div class="at-social-share">
  <?php if (!empty($current_path)) : ?>
    <div class="vkontakte">
      <script type="text/javascript"><!--
        document.write(VK.Share.button(false,{type: "round_nocount", text: (navigator.language.indexOf('ru') < 0) ? "Share" : "Сохранить", eng: 1}));
        --></script>
    </div>
    <div class="facebook">
      <div class="fb-share-button" data-href="<?php print $current_path; ?>" data-layout="button"></div>
    </div>
    <div class="twitter">
      <a class="twitter-share-button" href="https://twitter.com/intent/tweet?url=<?php print urlencode($current_path); ?>" data-size="small" data-count="none">
        Tweet
      </a>
    </div>
    <div class="google-plus">
      <div class="g-plus" data-action="share" data-annotation="none" data-height="20" data-href="<?php print urlencode($current_path); ?>"></div>
    </div>
  <?php endif; ?>
</div>
