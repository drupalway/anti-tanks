<?php
/**
 * @file
 * Template for the AntiTanks FTA + Tweets block.
 */
if (!empty($fta) && !empty($tweets)) : ?>
<div id="at_fta_and_tweets_block">
  <div class="at-fta-block">
    <h2 class="block-title">
      <?php print $fta['subject']; ?>
    </h2>
    <?php print render($fta['content']); ?>
  </div>
  <div class="at-tweets-block">
    <h2 class="block-title">
      <?php print $tweets['subject']; ?>
    </h2>
    <?php print render($tweets['content']); ?>
  </div>
</div>
<?php endif; ?>
