<?php
/**
 * @file
 * at_recepie.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function at_recepie_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'frontpage';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'at_wt_calculator-at_wt_calculator' => array(
          'module' => 'at_wt_calculator',
          'delta' => 'at_wt_calculator',
          'region' => 'content',
          'weight' => '-10',
        ),
        'at_fta_tweets_block-at_fta_tweets_block' => array(
          'module' => 'at_fta_tweets_block',
          'delta' => 'at_fta_tweets_block',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-tanks_demotivators-block' => array(
          'module' => 'views',
          'delta' => 'tanks_demotivators-block',
          'region' => 'content',
          'weight' => '-8',
        ),
        'views-f3b7b1f825389246c93dc50195a3ce6e' => array(
          'module' => 'views',
          'delta' => 'f3b7b1f825389246c93dc50195a3ce6e',
          'region' => 'content',
          'weight' => '-7',
        ),
        'views-recepies-block_2' => array(
          'module' => 'views',
          'delta' => 'recepies-block_2',
          'region' => 'content',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['frontpage'] = $context;

  return $export;
}
