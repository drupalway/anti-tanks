<?php
/**
 * @file
 * at_recepie.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function at_recepie_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|at_game|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'at_game';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_logo',
      ),
    ),
    'fields' => array(
      'field_logo' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|at_game|teaser'] = $ds_layout;

  return $export;
}
