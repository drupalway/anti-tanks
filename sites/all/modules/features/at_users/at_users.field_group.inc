<?php
/**
 * @file
 * at_users.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function at_users_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_parent|user|user|form';
  $field_group->group_name = 'group_parent';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Parent',
    'weight' => '0',
    'children' => array(
      0 => 'group_tanking',
      1 => 'group_main',
      2 => 'group_profile',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_parent|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile|user|user|form';
  $field_group->group_name = 'group_profile';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_parent';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '12',
    'children' => array(
      0 => 'field_full_name',
      1 => 'field_gender',
      2 => 'field_birthdate',
      3 => 'picture',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-profile field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_profile|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tanking|user|user|form';
  $field_group->group_name = 'group_tanking';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_parent';
  $field_group->data = array(
    'label' => 'Tanking',
    'weight' => '13',
    'children' => array(
      0 => 'field_status',
      1 => 'field_started_playing',
      2 => 'field_quited_playing',
      3 => 'field_you_are',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_tanking|user|user|form'] = $field_group;

  return $export;
}
