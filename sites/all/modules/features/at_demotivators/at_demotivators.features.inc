<?php
/**
 * @file
 * at_demotivators.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function at_demotivators_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function at_demotivators_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function at_demotivators_image_default_styles() {
  $styles = array();

  // Exported image style: demotiv.
  $styles['demotiv'] = array(
    'label' => 'demotiv',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 220,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: demotiv_node.
  $styles['demotiv_node'] = array(
    'label' => 'demotiv_node',
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 800,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      5 => array(
        'name' => 'canvasactions_file2canvas',
        'data' => array(
          'xpos' => 'right',
          'ypos' => 'bottom',
          'alpha' => 70,
          'path' => 'sites/all/themes/at/images/watermark.png',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: demotiv_roll.
  $styles['demotiv_roll'] = array(
    'label' => 'demotiv_roll',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 180,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
