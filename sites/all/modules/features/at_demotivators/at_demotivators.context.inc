<?php
/**
 * @file
 * at_demotivators.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function at_demotivators_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'demotiv_node';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'demotivators' => 'demotivators',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'at_images_roller-demotivators_roller' => array(
          'module' => 'at_images_roller',
          'delta' => 'demotivators_roller',
          'region' => 'content',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['demotiv_node'] = $context;

  return $export;
}
