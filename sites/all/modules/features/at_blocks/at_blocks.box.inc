<?php
/**
 * @file
 * at_blocks.box.inc
 */

/**
 * Implements hook_default_box().
 */
function at_blocks_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'social_icons';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Block with social icons bottom';
  $box->options = array(
    'body' => array(
      'value' => '<div class=social-icons-wrapper><a class="facebook" href="https://www.facebook.com/antitanks" target="_blank" alt="AntiTanks Facebook" title="AntiTanks Facebook"></a><a class="twitter" href="https://twitter.com/antitanks_net" target="_blank" alt="AntiTanks Twitter" title="AntiTanks Twitter"></a><a class="google-plus" href="https://plus.google.com/communities/108758775537635585111" target="_blank" alt="AntiTanks Google+" title="AntiTanks Google+"></a><a class="vk" href="https://vk.com/antitanks" target="_blank" alt="AntiTanks VK" title="AntiTanks VK"></a><a class="youtube" href="https://www.youtube.com/channel/UC62mHRx0fO1ICcNHFE_Qf-g" target="_blank" alt="AntiTanks YouTube" title="AntiTanks YouTube"></a></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['social_icons'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'social_icons_top';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Block with social icons top';
  $box->options = array(
    'body' => array(
      'value' => '<div class=social-icons-wrapper><a class="facebook" href="https://www.facebook.com/antitanks" target="_blank" alt="AntiTanks Facebook" title="AntiTanks Facebook"></a><a class="twitter" href="https://twitter.com/antitanks_net" target="_blank" alt="AntiTanks Twitter" title="AntiTanks Twitter"></a><a class="google-plus" href="https://plus.google.com/communities/108758775537635585111" target="_blank" alt="AntiTanks Google+" title="AntiTanks Google+"></a><a class="vk" href="https://vk.com/antitanks" target="_blank" alt="AntiTanks VK" title="AntiTanks VK"></a><a class="youtube" href="https://www.youtube.com/channel/UC62mHRx0fO1ICcNHFE_Qf-g" target="_blank" alt="AntiTanks YouTube" title="AntiTanks YouTube"></a></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['social_icons_top'] = $box;

  return $export;
}
