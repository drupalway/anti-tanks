<?php
/**
 * @file
 * at_basic.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function at_basic_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => '1000-2999',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '4b1abaa3-dd69-4bee-825d-05f4dc608e49',
    'vocabulary_machine_name' => 'battle_ranges',
    'field_battle_min_max' => array(
      'und' => array(
        0 => array(
          'from' => 1000,
          'to' => 2999,
        ),
      ),
    ),
    'field_tags' => array(),
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Wife',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '4edd59a0-57ae-4b2e-91cd-399cd91f3628',
    'vocabulary_machine_name' => 'fta_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => '10000-19999',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => '876bc581-f58d-4489-8934-3c9dab0062ed',
    'vocabulary_machine_name' => 'battle_ranges',
    'field_battle_min_max' => array(
      'und' => array(
        0 => array(
          'from' => 10000,
          'to' => 19999,
        ),
      ),
    ),
    'field_tags' => array(),
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => '3000-9999',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'a2eaa00f-6b33-4a0c-8f2d-1c3cff5fac47',
    'vocabulary_machine_name' => 'battle_ranges',
    'field_battle_min_max' => array(
      'und' => array(
        0 => array(
          'from' => 3000,
          'to' => 9999,
        ),
      ),
    ),
    'field_tags' => array(),
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Chief',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => 'a3b75726-f322-4935-bca0-f5009b70fb7c',
    'vocabulary_machine_name' => 'fta_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Parents',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'a4dd74da-11a1-4fb8-bb40-2ee3296ce2a0',
    'vocabulary_machine_name' => 'fta_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => '20000-100000000000000',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => 'db5f0164-623e-443f-aa81-f07c22af8ea7',
    'vocabulary_machine_name' => 'battle_ranges',
    'field_battle_min_max' => array(
      'und' => array(
        0 => array(
          'from' => 20000,
          'to' => 1000000000,
        ),
      ),
    ),
    'field_tags' => array(),
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => '1-999',
    'description' => 'testrange',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e20b593d-4249-497b-9f7d-9879f597b50d',
    'vocabulary_machine_name' => 'battle_ranges',
    'field_battle_min_max' => array(
      'und' => array(
        0 => array(
          'from' => 1,
          'to' => 999,
        ),
      ),
    ),
    'field_tags' => array(),
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Tankman',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'f034d254-0e2a-492c-993d-e4b5222e235d',
    'vocabulary_machine_name' => 'fta_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Friends',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'fd7604ad-6384-4ab5-a86e-bac85dcd1b49',
    'vocabulary_machine_name' => 'fta_category',
    'metatags' => array(),
  );
  return $terms;
}
