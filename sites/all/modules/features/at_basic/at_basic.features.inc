<?php
/**
 * @file
 * at_basic.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function at_basic_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function at_basic_node_info() {
  $items = array(
    'conclusions' => array(
      'name' => t('Conclusions'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'demotivators' => array(
      'name' => t('Demotivators'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'fta' => array(
      'name' => t('FTA'),
      'base' => 'node_content',
      'description' => t('Fucking antiTanks Advices'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'recepies' => array(
      'name' => t('Recepies'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
