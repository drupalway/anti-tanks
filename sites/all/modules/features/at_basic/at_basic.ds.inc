<?php
/**
 * @file
 * at_basic.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function at_basic_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|fta|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'fta';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        'col-xs-10 col-sm-8 col-md-10 col-lg-10' => 'col-xs-10 col-sm-8 col-md-10 col-lg-10',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|fta|default'] = $ds_layout;

  return $export;
}
