<?php
/**
 * @file
 * at_basic.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function at_basic_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*' => '*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-social_icons_top' => array(
          'module' => 'boxes',
          'delta' => 'social_icons_top',
          'region' => 'navigation',
          'weight' => '-10',
        ),
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'boxes-social_icons' => array(
          'module' => 'boxes',
          'delta' => 'social_icons',
          'region' => 'footer',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['global'] = $context;

  return $export;
}
