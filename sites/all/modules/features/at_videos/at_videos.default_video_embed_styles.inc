<?php
/**
 * @file
 * at_videos.default_video_embed_styles.inc
 */

/**
 * Implements hook_default_video_embed_styles().
 */
function at_videos_default_video_embed_styles() {
  $export = array();

  $video_embed_style = new stdClass();
  $video_embed_style->disabled = FALSE; /* Edit this to true to make a default video_embed_style disabled initially */
  $video_embed_style->api_version = 1;
  $video_embed_style->name = 'antitanks';
  $video_embed_style->title = 'AntiTanks';
  $video_embed_style->data = array(
    'facebook' => array(
      'height' => '100%',
      'width' => '640',
    ),
    'youtube' => array(
      'width' => '100%',
      'height' => '640',
      'theme' => 'dark',
      'autoplay' => 0,
      'vq' => 'large',
      'rel' => 1,
      'showinfo' => 1,
      'modestbranding' => 0,
      'iv_load_policy' => '1',
      'controls' => '1',
      'autohide' => '2',
    ),
    'vimeo' => array(
      'width' => '100%',
      'height' => '640',
      'color' => '00adef',
      'portrait' => 1,
      'title' => 1,
      'byline' => 1,
      'autoplay' => 0,
      'loop' => 0,
      'froogaloop' => 0,
    ),
    'data__active_tab' => 'edit-data-youtube',
  );
  $export['antitanks'] = $video_embed_style;

  $video_embed_style = new stdClass();
  $video_embed_style->disabled = FALSE; /* Edit this to true to make a default video_embed_style disabled initially */
  $video_embed_style->api_version = 1;
  $video_embed_style->name = 'antitanks_frontpage';
  $video_embed_style->title = 'AntiTanks Frontpage';
  $video_embed_style->data = array(
    'facebook' => array(
      'height' => '240',
      'width' => '360',
    ),
    'youtube' => array(
      'width' => '360',
      'height' => '240',
      'theme' => 'dark',
      'autoplay' => 0,
      'vq' => 'large',
      'rel' => 0,
      'showinfo' => 1,
      'modestbranding' => 0,
      'iv_load_policy' => '1',
      'controls' => '1',
      'autohide' => '2',
    ),
    'vimeo' => array(
      'width' => '360',
      'height' => '240',
      'color' => '00adef',
      'portrait' => 1,
      'title' => 1,
      'byline' => 1,
      'autoplay' => 0,
      'loop' => 0,
      'froogaloop' => 0,
    ),
    'data__active_tab' => 'edit-data-vimeo',
  );
  $export['antitanks_frontpage'] = $video_embed_style;

  return $export;
}
