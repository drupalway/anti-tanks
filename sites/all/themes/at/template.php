<?php
/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * at theme.
 */

require_once drupal_get_path('theme', 'bootstrap') . '/theme/process.inc';

/**
 * Implements hook_form_alter().
 */
function at_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'comment_node_demotivators_form' || $form_id === 'comment_node_recepies_form') {
    $form['#after_build'][] = 'at_comments_format_form_after_build';
  }
}

/**
 * Custom after-build callback.
 */
function at_comments_format_form_after_build($form) {
  $form['comment_body']['und']['0']['format']['#access'] = FALSE;
  return $form;
}

/**
 * Implements hook_js_alter().
 */
function at_js_alter(&$js) {
  $files = array(
    '//code.jquery.com/ui/1.10.2/jquery-ui.js',
    'sites/all/modules/contrib/back_to_top/js/back_to_top.js',
    'sites/all/modules/contrib/jquery_update/replace/ui/external/jquery.cookie.js',
    'sites/all/modules/contrib/jquery_update/replace/misc/jquery.form.js',
    'sites/all/modules/contrib/jquery_update/js/jquery_update.js',
    'sites/all/modules/custom/at_wt_calculator/at_wt_calculator.js',
  );
  foreach ($files as $file) {
    if (isset($js[$file])) {
      $js[$file]['scope'] = 'footer';
      $js[$file]['async'] = TRUE;
    }
  }
  // Remove local bootstrap.js.
  if (isset($js['sites/all/themes/bootstrap/js/bootstrap.js'])) {
    unset($js['sites/all/themes/bootstrap/js/bootstrap.js']);
  }
}
