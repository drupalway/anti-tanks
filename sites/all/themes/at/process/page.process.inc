<?php
/**
 * @file
 * Page process hooks file definition.
 */

/**
 * Implements hook_process_page().
 */
function at_process_page(&$vars) {
  // You can use process hooks to modify the variables before they are passed to
  // the theme function or template file.
}
