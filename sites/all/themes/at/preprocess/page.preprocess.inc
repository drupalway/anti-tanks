<?php
/**
 * @file
 * Page preprocess hooks file definition.
 */

/**
 * Implements hook_preprocess_page().
 */
function at_preprocess_page(&$vars) {
  // You can use preprocess hooks to modify the variables
  // before they are passed to the theme function or template file.
}
