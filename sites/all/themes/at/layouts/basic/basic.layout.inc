name = Basic at Layout
description = A 3 column layout with a prominent hero region.
preview = preview.png
template = basic-layout

; Regions
regions[branding]       = Branding
regions[navigation]     = Navigation bar
regions[header]         = Header
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/less.css
stylesheets[all][] = css/at.styles.css
